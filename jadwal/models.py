from django.db import models

# Create your models here.
class jadwal_kuliah(models.Model):
    mata_kuliah = models.CharField(blank=False, max_length= 100)
    dosen_pengajar = models.CharField(blank=False, max_length= 100)
    jumlah_sks = models.CharField(blank=False, max_length= 100)
    jam_kelas = models.CharField(blank=False, max_length= 100)
    semester_tahun = models.CharField(blank=False, max_length= 100)
    deskripsi = models.CharField(blank=False, max_length= 100, default='0000000')