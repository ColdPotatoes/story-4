from django.urls import path
from . import views

app_name = 'jadwal'

urlpatterns = [
    path('', views.jadwal_view,name='jadwal_url'),
    path('jadwal/', views.jadwal_view),
    path('detail/<int:pk>',views.detail, name="detail"),
    path('<int:pk>', views.delete, name="hapus"),

  
]