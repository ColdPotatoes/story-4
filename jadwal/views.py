from django.shortcuts import render, redirect
from . import forms, models


# Create your views here.
def jadwal_view(request):
    if(request.method == "POST"):
        tmp = forms.formulirku(request.POST)
        if(tmp.is_valid()):
            model_jadwal = models.jadwal_kuliah()
            model_jadwal.mata_kuliah = tmp.cleaned_data["mata_kuliah"]
            model_jadwal.dosen_pengajar= tmp.cleaned_data["dosen_pengajar"]
            model_jadwal.jumlah_sks = tmp.cleaned_data["jumlah_sks"]
            model_jadwal.jam_kelas = tmp.cleaned_data["jam_kelas"]
            model_jadwal.semester_tahun = tmp.cleaned_data["semester_tahun"]
            model_jadwal.deskripsi = tmp.cleaned_data["deskripsi"]
            model_jadwal.save()
        return redirect("/jadwal")
    else:
        form = forms.formulirku()
        data_matkul = models.jadwal_kuliah.objects.all()
        context = {
            'formulir' : form,
            'jadwal' : data_matkul
        }
        return render(request, 'jadwalku.html', context)

#untuk delete 
def delete(request, pk):
    if (request.method == "POST"):
        form = forms.formulirku(request.POST)
        if(form.is_valid()):
            model_jadwal = models.jadwal_kuliah()
            model_jadwal.mata_kuliah = form.cleaned_data["mata_kuliah"]
            model_jadwal.dosen_pengajar = form.cleaned_data["dosen_pengajar"]
            model_jadwal.jumlah_sks = form.cleaned_data["jumlah_sks"]
            model_jadwal.jam_kelas = form.cleaned_data["jam_kelas"]
            model_jadwal.semester_tahun= form.cleaned_data["semester_tahun"]
            model_jadwal.deskripsi= form.cleaned_data["deskripsi"]
            model_jadwal.save()
        return redirect ("/jadwal")
    else:
        models.jadwal_kuliah.objects.filter(pk = pk).delete()
        form = forms.formulirku()
        data_matkul = models.jadwal_kuliah.objects.all()
        context_delete ={
            "jadwal" : data_matkul,
            "formulir": form
        }
        return render(request, 'jadwalku.html', context_delete)


#detail
def detail(request, pk):
    matkul = models.jadwal_kuliah.objects.get(pk=pk)
    context_detail= {
        "matkul" : matkul,

    }
    return render(request, 'detail.html', context_detail)


