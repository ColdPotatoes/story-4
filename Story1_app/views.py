from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'Story1.html')

def about(request):
    return render(request, 'Story1_About.html')